#!/bin/sh

python manage.py collectstatic --noinput --ignore libs/imagesloaded/sandbox/background/*
python manage.py compress --force

python manage.py migrate
gunicorn config.wsgi:application --bind=0.0.0.0:80

from django.db import models
from django.utils.translation import ugettext_lazy as _

from crawfish.models import BaseModel


class Currency(BaseModel):

    code = models.CharField(_("Currency Code"), max_length=3, unique=True)
    symbol = models.CharField(_("Symbol"), max_length=1)

    def __str__(self):
        return (
            f"Currency(code: '{self.code}', symbol: '{self.symbol}', "
            f"exchange_rate: '{self.exchangerate_set.order_by('updated_at').first()}')"
        )

    class Meta:
        verbose_name = "Currency"
        verbose_name_plural = "Currencies"


class ExchangeRate(BaseModel):
    usd = models.FloatField(_("USD per"))

    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)

    def __str__(self):
        return f"ExchangeRate(usd: '{self.usd}')"

    class Meta:
        verbose_name = "Exchange Rate"
        verbose_name_plural = "Exchange Rates"

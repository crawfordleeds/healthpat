from healthpat.currencies.tasks import update_exchange_rates
from healthpat.currencies.models import Currency, ExchangeRate
from django.test import TestCase
from unittest.mock import patch


class TasksIntegrationTests(TestCase):
    @patch(
        "healthpat.currencies.tasks.get_mxn_exchange_rate",
    )
    def test_update_exchange_rates(self, mocked_get_mxn):
        """
        Test the complete update_exchange_rates method starting in a null state (no currency/exchange rate records)
        """
        mocked_get_mxn.return_value = 20
        self.assertEqual(Currency.objects.count(), 0)
        self.assertEqual(ExchangeRate.objects.count(), 0)
        update_exchange_rates()
        self.assertTrue(mocked_get_mxn.called)
        self.assertEqual(mocked_get_mxn.call_count, 1)

        self.assertEqual(Currency.objects.count(), 3)
        self.assertEqual(ExchangeRate.objects.count(), 3)

        # Check the BTC Record
        btc_filter = Currency.objects.filter(code="BTC")
        self.assertEqual(btc_filter.count(), 1)
        btc = btc_filter.first()
        self.assertEqual(btc.symbol, "₿")
        self.assertEqual(btc.exchangerate_set.count(), 1)
        # Check the result falls within the range of expectations
        self.assertGreaterEqual(btc.exchangerate_set.first().usd, 1)
        self.assertLessEqual(btc.exchangerate_set.first().usd, 100000)

        # Check the USD Record
        usd_filter = Currency.objects.filter(code="USD")
        self.assertEqual(usd_filter.count(), 1)
        usd = usd_filter.first()
        self.assertEqual(usd.symbol, "$")
        self.assertEqual(usd.exchangerate_set.count(), 1)
        # 1 USD = 1 USD
        self.assertEqual(usd.exchangerate_set.first().usd, 1)

        # Check the MXN Record
        mxn_filter = Currency.objects.filter(code="MXN")
        self.assertEqual(mxn_filter.count(), 1)
        mxn = mxn_filter.first()
        self.assertEqual(mxn.symbol, "$")
        self.assertEqual(mxn.exchangerate_set.count(), 1)
        # Check the result falls within the range of expectations.
        self.assertGreaterEqual(mxn.exchangerate_set.first().usd, 1)
        self.assertLessEqual(mxn.exchangerate_set.first().usd, 50)

    @patch(
        "healthpat.currencies.tasks.get_mxn_exchange_rate",
    )
    def test_update_exchange_rates__update(self, mocked_get_mxn):
        """
        Test the complete update_exchange_rates method starting in a state with previous currency/exchange rate records
        """
        mocked_get_mxn.return_value = 20

        # Load records into the test db
        update_exchange_rates()
        self.assertTrue(mocked_get_mxn.called)
        self.assertEqual(mocked_get_mxn.call_count, 1)

        self.assertEqual(Currency.objects.count(), 3)
        self.assertEqual(ExchangeRate.objects.count(), 3)

        # Update records again and ensure records were updated, not appended
        update_exchange_rates()
        self.assertTrue(mocked_get_mxn.called)
        self.assertEqual(mocked_get_mxn.call_count, 2)
        self.assertEqual(Currency.objects.count(), 3)
        self.assertEqual(ExchangeRate.objects.count(), 3)

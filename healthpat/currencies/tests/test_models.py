from django.test import TestCase

from healthpat.currencies.models import Currency, ExchangeRate


class BaseTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.code = "TES"
        cls.symbol = "$"
        cls.usd_exchange_rate = 123


class CurrencyTests(BaseTests):
    def create(self):
        return Currency.objects.create(code="TES", symbol="$")

    def test_create(self):
        currency = self.create()
        self.assertIsInstance(currency, Currency)
        self.assertEqual(Currency.objects.count(), 1)

    def test_delete(self):
        self.assertEqual(Currency.objects.count(), 0)
        currency = self.create()
        self.assertEqual(Currency.objects.count(), 1)
        currency.delete()
        self.assertEqual(Currency.objects.count(), 0)


class ExchangeRateTests(BaseTests):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.currency = Currency.objects.create(code=cls.code, symbol=cls.symbol)

    def create(self):
        return ExchangeRate.objects.create(
            usd=self.usd_exchange_rate, currency=self.currency
        )

    def test_create(self):
        exchange_rate = self.create()
        self.assertIsInstance(exchange_rate, ExchangeRate)
        self.assertEqual(ExchangeRate.objects.count(), 1)

    def test_delete(self):
        exchange_rate = self.create()
        self.assertEqual(ExchangeRate.objects.count(), 1)
        exchange_rate.delete()
        self.assertEqual(ExchangeRate.objects.count(), 0)

from django.test import TestCase

from healthpat.currencies.models import Currency, ExchangeRate
from healthpat.currencies.serializers import CurrencySerializer, ExchangeRateSerializer


class CurrencySerializerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.code = "TES"
        cls.symbol = "$"

    def test_create(self):
        serializer = CurrencySerializer(data={"code": self.code, "symbol": self.symbol})
        self.assertTrue(serializer.is_valid())
        res = serializer.save()
        self.assertIsInstance(res, Currency)
        self.assertEqual(res.code, self.code)
        self.assertEqual(res.symbol, self.symbol)
        self.assertEqual(res.exchangerate_set.count(), 0)


class ExchangeRateSerializerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.currency = Currency.objects.create(code="TES", symbol="$")

    def test_create(self):
        usd = 123
        serializer = ExchangeRateSerializer(
            data={"currency": self.currency.id, "usd": usd}
        )
        self.assertTrue(serializer.is_valid())
        res = serializer.save()
        self.assertIsInstance(res, ExchangeRate)
        self.assertEqual(res.usd, usd)
        self.assertEqual(res.currency, self.currency)

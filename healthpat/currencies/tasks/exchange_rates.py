import requests
from crawfish.utils import raise_
from healthpat.coinmarketcap.exceptions import InvalidRequest
from healthpat.coinmarketcap import coinmarketcap
import logging
from rest_framework import status

logger = logging.getLogger(__name__)


# BTC
# ------------------------------------------------------------------------------
def get_btc_exchange_rate():
    def cmc_error():
        raise_(InvalidRequest("Unable to obtain Bitcoin quote."))

    # TODO: Update when coinmarketcap sdk returns an object abstraction for the full schema instead of
    # a dict
    return (
        coinmarketcap.Quotes.retrieve(symbol="BTC")
        .quote.get("USD", lambda: cmc_error())
        .get("price", lambda: cmc_error())
    )


# MXN
# ------------------------------------------------------------------------------
def get_mxn_exchange_rate():
    base_url = "https://api.exchangeratesapi.io/latest?base=USD&symbols=MXN"

    try:
        resp = requests.get(base_url)
        if resp.status_code != status.HTTP_200_OK:
            logging.info(
                f"exchangeratesapi error. Response status code: {resp.status_code}."
            )
            raise Exception("Unable to obtain USD/MXN exchange rate.")
    except Exception as e:
        logger.exception(e)
        raise e

    return (
        resp.json()
        .get("rates", {})
        .get(
            "MXN",
            lambda: raise_(
                Exception(
                    "Cannot extract USD/MXN exchange rate from exchangeratesapi data."
                )
            ),
        )
    )


# USD
# ------------------------------------------------------------------------------
def get_usd_exchange_rate():
    """
    1 USD = 1 USD
    """
    return 1

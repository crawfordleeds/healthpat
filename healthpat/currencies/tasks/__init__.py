from config import celery_app

from healthpat.currencies.models import Currency, ExchangeRate
from healthpat.currencies.serializers import CurrencySerializer

from .exchange_rates import (
    get_btc_exchange_rate,
    get_mxn_exchange_rate,
    get_usd_exchange_rate,
)

BTC = dict(code="BTC", symbol="₿")
MXN = dict(code="MXN", symbol="$")
USD = dict(code="USD", symbol="$")

SUPPORTED_CURRENCIES = (
    BTC,
    USD,
    MXN,
)


def get_exchange_rate(code):
    """Return the current exchange rate based on the currency code (e.g. USD)"""
    return {
        "BTC": get_btc_exchange_rate,
        "MXN": get_mxn_exchange_rate,
        "USD": get_usd_exchange_rate,
    }.get(code, lambda: None)()


@celery_app.task()
def update_exchange_rates():
    """
    Update site wide currency exchange rates.

    Ensure currencies (as defined above) are properly recorded in the db
    """

    # Check for all relevant currencies in the database
    for currency in SUPPORTED_CURRENCIES:
        try:
            Currency.objects.get(code=currency["code"])
        except Currency.DoesNotExist as dne:
            serializer = CurrencySerializer(data=currency)
            if serializer.is_valid():
                serializer.save()

    # Update Exchange Rates
    for c in Currency.objects.all():
        if not c.exchangerate_set.count():
            ExchangeRate.objects.create(currency=c, usd=get_exchange_rate(c.code))
        else:
            exchange_rate = (
                ExchangeRate.objects.filter(currency=c).order_by("updated_at").first()
            )
            exchange_rate.usd = get_exchange_rate(c.code)
            exchange_rate.save()

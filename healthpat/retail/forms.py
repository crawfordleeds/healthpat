from django import forms
from .models import NewDrugsRequested

from crispy_forms.helper import FormHelper

# New Drug Request Form
class NewDrugRequestForm(forms.ModelForm):
    class Meta:
        model = NewDrugsRequested
        fields = "__all__"

    @property
    def helper(self):
        helper = FormHelper(self)
        helper.label_class = "form-control-label"
        helper.form_tag = False  # Prevent crispy form from adding form tag. We do this manually in HTML to get more control over the submit button.
        return helper

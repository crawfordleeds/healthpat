from django.urls import path


from . import views

app_name = "retail"
urlpatterns = [
    path("", views.HomePageView.as_view(), name="home"),
    path("request/", views.RequestNewDrugView.as_view(), name="request"),
]

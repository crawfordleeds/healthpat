from django.db import models
from django.utils.translation import ugettext_lazy as _

from multiselectfield import MultiSelectField
from crawfish.models import EmailField, BaseModel


class NewDrugsRequested(BaseModel):
    class Meta:
        verbose_name = "New Drugs Requested"
        verbose_name_plural = "New Drugs Requested"

    drug_name = models.CharField(_("Drug Name"), max_length=200)
    current_problems = MultiSelectField(
        _("Current Problems Obtaining Medication"),
        choices=(
            ("too_expensive", "Too Expensive"),
            ("availability", "Availability"),
            ("other", "Other"),
        ),
        blank=True,
    )
    email = EmailField(_("Email"), max_length=255, blank=True)

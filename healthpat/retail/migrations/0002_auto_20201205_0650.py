# Generated by Django 3.1.3 on 2020-12-05 06:50

import crawfish.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('retail', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newdrugsrequested',
            name='email',
            field=crawfish.models.fields.EmailField(blank=True, max_length=255, verbose_name='Email'),
        ),
    ]

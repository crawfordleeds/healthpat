from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered

from .models import NewDrugsRequested


class NewDrugsRequestedAdmin(admin.ModelAdmin):

    list_display = (
        "id",
        "drug_name",
        "current_problems",
        "email",
    )

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "drug_name",
                    "current_problems",
                    "email",
                )
            },
        ),
    )
    search_fields = (
        "drug_name",
        "email",
    )


try:
    admin.site.register(NewDrugsRequested, NewDrugsRequestedAdmin)
except AlreadyRegistered:
    pass

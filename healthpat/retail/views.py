from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy

from .forms import NewDrugRequestForm


class HomePageView(TemplateView):
    """Render the home page"""

    template_name = "retail/home.html"
    extra_context = {"settings": settings, "header_style": "primary"}


class RequestNewDrugView(SuccessMessageMixin, CreateView):
    """Render a page to collect requests to index new drugs via form submission"""

    template_name = "retail/requests.html"
    extra_context = {"header_style": "primary"}
    form_class = NewDrugRequestForm
    success_url = reverse_lazy("retail:request")
    success_message = "Thanks for your message!"

import logging

from django.apps import apps
from config import celery_app

from . import algolia_index

logger = logging.getLogger(__name__)


@celery_app.task()
def save_object(app_name, model_name, record_id):
    """
    Save an existing object in Algolia, or create one if an objectID does not exist
    """
    logging.info(
        f"Saving model {app_name}.{model_name} with record id {record_id} to Algolia..."
    )
    model = apps.get_model(f"{app_name}.{model_name}")
    obj = model.objects.get(pk=record_id)

    logger.info(f"Saving record, {obj} to alolia index, {algolia_index}")

    # Will add if no objectID, otherwise update record
    resp = algolia_index.save_object(
        obj.to_json(), {"autoGenerateObjectIDIfNotExist": True}
    )
    object_id = resp.raw_responses[0].get("objectIDs")[0]
    obj.objectID = object_id
    obj.save(
        save_to_algolia=False
    )  # Update the object with the object ID but don't re-save to algolia.

    logger.info(
        f"Finished saving record in Algolia for record, {record_id} in model, {model}."
    )


@celery_app.task()
def delete_object(object_id: str):
    """Delete a record from Algolia"""

    algolia_index.delete_object(object_id)


@celery_app.task()
def configure_dataset():
    """
    (WIP) Configure the dataset
    """
    try:
        algolia_index.set_settings(
            {
                "searchableAttributes": [
                    "brand",
                    "name",
                    "categories",
                    "unordered(description)",
                ],
                "customRanking": ["desc(popularity)"],
                "attributesForFaceting": [
                    "searchable(brand)",
                    "type",
                    "categories",
                    "price",
                ],
            }
        )
    except Exception as e:
        logger.exception(e)


@celery_app.task()
def check_for_unrecorded_products():
    """
    Check for any products in the db without an object ID and add them to Algolia.
    """
    from healthpat.products.models import Products

    products = Products.objects.filter(objectID="")
    logger.info(
        f"Found {products.count()} records without an Algolia objectID. Adding...."
    )
    for p in products:
        save_object.delay("products", "Products", p.id)

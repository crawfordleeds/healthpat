from django.apps import AppConfig


class AlgoliaConfig(AppConfig):
    name = "algolia"

from django.conf import settings

from algoliasearch.search_client import SearchClient

algolia_client = SearchClient.create(
    app_id=settings.ALGOLIA_APPLICATION_ID, api_key=settings.ALGOLIA_API_KEY
)
algolia_index = algolia_client.init_index(settings.ALGOLIA_INDEX_NAME)

from abc import ABC


class BaseError(ABC):
    pass


class Error(BaseError):
    pass

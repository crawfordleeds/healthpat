import datetime
import random
import string
from unittest import mock
from unittest.mock import PropertyMock, patch

from django.test import TestCase
from django.utils import timezone

import pytz

from healthpat.coinmarketcap import BaseHandler, CoinMarketCap, Quotes, coinmarketcap
from healthpat.coinmarketcap.exceptions import InvalidRequest


def get_random_symbol():
    symbol = "".join(random.choice(string.ascii_uppercase) for _ in range(3))
    if symbol == "BTC":
        get_random_symbol()
    return symbol


class CMCTests(TestCase):
    def test_instantiation(self):
        self.assertIsInstance(coinmarketcap, CoinMarketCap)


class QuotesTests(TestCase):
    def test_instantiation(self):
        self.assertIsInstance(coinmarketcap.Quotes, Quotes)

    def test_retrieve(self):
        """Test the retrieve method to obtain the most recent market quotes"""
        res = coinmarketcap.Quotes.retrieve()

        self.assertIsInstance(res, Quotes)
        self.assertEqual(res.name, "Bitcoin")
        self.assertEqual(res.symbol, "BTC")
        self.assertEqual(res.date_added, "2013-04-28T00:00:00.000Z")

        # Check for rough accuracy of "last updated" by ensuring the datetime received is less than the current
        # datetime but greater than a datetime 1 day in the past
        self.assertLessEqual(
            datetime.datetime.strptime(
                res.last_updated, "%Y-%m-%dT%H:%M:%S.%fZ"
            ).astimezone(pytz.UTC),
            timezone.now().astimezone(pytz.UTC),
        )
        self.assertGreater(
            datetime.datetime.strptime(
                res.last_updated, "%Y-%m-%dT%H:%M:%S.%fZ"
            ).astimezone(pytz.UTC),
            timezone.now().astimezone(pytz.UTC) - datetime.timedelta(days=1),
        )

    def test_retrieve__invalid_symbol(self):
        """
        Test the retrieve method with a currency symbol that is invalid
        """
        invalid_symbol = get_random_symbol()
        with self.assertRaises(InvalidRequest) as e:
            coinmarketcap.Quotes.retrieve(symbol=invalid_symbol)
        self.assertIsInstance(e.exception, InvalidRequest)
        self.assertEqual(e.exception.__str__(), "Why....?")

    def test_retrieve__cannot_map_symbol(self):
        """
        Test the retrieve method with a currency symbol that cannot be found in the mapping to
        the coinmarketcap id
        """
        with mock.patch.object(
            BaseHandler, "cmc_symbol_to_id", new_callable=PropertyMock
        ) as mock_cmc_symbol_to_id:
            mock_cmc_symbol_to_id.return_value = {}

            symbol = "BTC"
            with self.assertRaises(InvalidRequest) as e:
                coinmarketcap.Quotes.retrieve(symbol=symbol)

            self.assertIsInstance(e.exception, InvalidRequest)
            self.assertEqual(
                e.exception.__str__(), f"Cannot find CMC id for symbol {symbol}"
            )

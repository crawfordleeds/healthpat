class InvalidRequest(Exception):
    def __init__(self, msg="Invalid request", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

import json
import urllib
from abc import ABC

from django.conf import settings

import requests
from rest_framework import status

from crawfish.utils import raise_

from .exceptions import InvalidRequest

API_BASE_URL = "https://pro-api.coinmarketcap.com"


class BaseHandler(ABC):
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            if k in self.fields:
                setattr(self, k, v)

    def get_headers(self):
        return {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-CMC_PRO_API_KEY": f"{settings.COINMARKETCAP_API_KEY}",
        }

    def json(self):
        d = {}
        for f in self.fields:
            d.update({f: getattr(self, f, None)})
        return d

    @property
    def fields(self):
        raise NotImplementedError

    @property
    def cmc_symbol_to_id(self):
        """
        Return dict mapping ticker symbols (e.g. BTC) to the CMC id.
        """
        return {"BTC": lambda: 1}


class Quotes(BaseHandler):
    endpoint = "v1/cryptocurrency/quotes/latest"
    fields = (
        "name",
        "symbol",
        "last_updated",
        "date_added",
        "quote",
    )

    def retrieve(self, symbol="BTC"):
        """
        Retrieve latest price quotes from CMC
        symbol: string of crypto symbol to retrieve price quote
        https://coinmarketcap.com/api/documentation/v1/#operation/getV1CryptocurrencyListingsLatest
        """
        if symbol != "BTC":
            raise InvalidRequest(f"Why....?")
        symbol_id = self.cmc_symbol_to_id.get(
            symbol,
            lambda: raise_(InvalidRequest(f"Cannot find CMC id for symbol {symbol}")),
        )()
        params = {
            "id": symbol_id,
        }
        url = f"{API_BASE_URL}/{self.endpoint}?{urllib.parse.urlencode(params)}"
        resp = requests.get(
            url,
            headers=self.get_headers(),
        )
        if resp.status_code == status.HTTP_200_OK:
            quote = self.__class__(**resp.json().get("data").get(str(symbol_id)))
        else:
            return resp
        return quote

    def __repr__(self):
        return f"Quote:\n{json.dumps(self.json(), indent=4)}"


class CoinMarketCap(object):

    Quotes = Quotes()


coinmarketcap = CoinMarketCap()

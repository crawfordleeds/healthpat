from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import views as django_views
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from crawfish.emails import send_mail

from .forms import LoginForm, UserCreationForm, PasswordResetForm, SetPasswordForm


class LoginView(django_views.LoginView):
    extra_context = {"header_style": "primary"}
    form_class = LoginForm
    template_name = "users/login.html"


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            # Login the new user automatically
            email = form.cleaned_data.get("email")
            password = form.cleaned_data.get("password1")
            new_user = authenticate(username=email, password=password)
            login(request, new_user)
            send_mail(
                to_email=new_user.email,
                template_id="d-114c8baff3f34ade9fe2be663c4bf3f3",
            )
            messages.success(
                request,
                f"Welcome to HealthPat. Check your inbox to confirm your email.",
            )
            return redirect("dashboard:dashboard")
    else:
        form = UserCreationForm()

    return render(
        request, "users/signup.html", {"form": form, "header_style": "primary"}
    )


class PasswordResetView(django_views.PasswordResetView):
    extra_context = {"header_style": "primary"}
    success_url = reverse_lazy("users:password_reset_done")
    template_name = "users/password_reset.html"
    form_class = PasswordResetForm


class PasswordResetDoneView(django_views.PasswordResetDoneView):
    extra_context = {"header_style": "primary"}
    template_name = "users/password_reset_done.html"


class PasswordResetConfirmView(
    SuccessMessageMixin, django_views.PasswordResetConfirmView
):
    success_url = reverse_lazy("users:login")
    success_message = "You may now log in using your new password."
    extra_context = {"header_style": "primary"}
    template_name = "users/password_reset_confirm.html"
    form_class = SetPasswordForm

from django.urls import path, reverse_lazy
from django.contrib.auth.views import (
    LogoutView,
)
from . import views

app_name = "users"
urlpatterns = [
    path("signup", views.signup, name="register"),
    path(
        "login",
        views.LoginView.as_view(redirect_authenticated_user=True),
        name="login",
    ),
    path("logout", LogoutView.as_view(), name="logout"),
    path(
        "password-reset/",
        views.PasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "password-reset/done/",
        views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "password-reset-confirm/<uidb64>/<token>/",
        views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
]

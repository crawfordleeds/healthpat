from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from . import models
from .forms import UserChangeForm, UserCreationForm

User = get_user_model()


@admin.register(models.User)
class UserAdmin(BaseUserAdmin):

    ordering = ("email",)
    list_display = ["email", "uuid", "is_superuser"]
    fieldsets = (("User", {"fields": ("email",)}),)
    search_fields = ["email"]
    form = UserChangeForm

    add_form = UserCreationForm
    # Override default add_fieldsets to replace the username field with email
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "password1",
                    "password2",
                    "is_active",
                    "is_confirmed",
                ),  # is active required so superusers can create a user with is_active=False before ready to onboard
            },
        ),
    )

import unicodedata

from django.contrib.auth import forms, get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from crispy_forms.bootstrap import PrependedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Layout, MultiField, Submit
from crawfish.emails import send_mail

### LOGIN
class LoginForm(forms.AuthenticationForm):
    prepend_text = {
        # field_name: prepend_text
        "username": "<i class='fa fa-user'></i>",
        "password": "<i class='fa fa-key'></i>",
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.error_messages.update(
            {
                "invalid_login": _(
                    "Invalid %(username)s or password. Note that both "
                    "fields may be case-sensitive."
                ),
            }
        )
        self.fields["username"].empty_value = "test@example.com"
        self.fields["password"].empty_value = "Password"

    @property
    def helper(self):
        helper = FormHelper(self)
        helper.label_class = "form-control-label"
        helper.form_tag = False  # Prevent crispy form from adding form tag. We do this manually in HTML to get more control over the submit button.
        layout = helper.layout = Layout()
        # Apply placeholders and prepended text to input
        # https://stackoverflow.com/questions/13482753/use-field-label-as-placeholder-in-django-crispy-forms/23971724#23971724

        for field_name, field in self.fields.items():
            item = PrependedText(
                field_name,
                self.prepend_text.get(field_name),
                placeholder=field.empty_value,
            )

            if field_name == "password":
                """Apply the 'lost password' link"""
                item = HTML(
                    """<div class="form-group mb-4">
                      <div class="d-flex align-items-center justify-content-between">
                        <div>
                          <label class="form-control-label">Password*</label>
                        </div>
                        <div class="mb-2">
                          <a tabindex="-1" href="{% url 'users:password_reset' %}" class="small text-muted text-underline--dashed border-primary">Forgot password?</a>
                        </div>
                      </div>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-key"></i></span>
                        </div>
                        <input type="password" name="password" autocomplete="current-password" class="form-control" id="input-password" placeholder="Password">
                      </div>
                    </div>"""
                )
            layout.append(item)

        return helper


### {end} LOGIN


class UserChangeForm(forms.UserChangeForm):
    class Meta(forms.UserChangeForm.Meta):
        model = get_user_model()


class EmailField(forms.UsernameField):
    def to_python(self, value):
        return unicodedata.normalize("NFKC", super().to_python(value))

    def widget_attrs(self, widget):
        return {
            **super().widget_attrs(widget),
            "autocapitalize": "none",
            "autocomplete": "email",
        }


class UserCreationForm(forms.UserCreationForm):
    prepend_text = {
        # field_name: prepend_text
        "email": "<i class='fa fa-user'></i>",
        "password1": "<i class='fa fa-key'></i>",
        "password2": "<i class='fa fa-key'></i>",
    }
    error_message = forms.UserCreationForm.error_messages.update(
        {"duplicate_email": _("This email has already been taken.")}
    )

    class Meta(forms.UserCreationForm.Meta):
        model = get_user_model()
        fields = ("email",)
        field_classes = {"email": EmailField}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["email"].empty_value = "test@example.com"
        self.fields["password1"].empty_value = "**********"
        self.fields["password2"].empty_value = "**********"

    @property
    def helper(self):
        helper = FormHelper(self)
        helper.label_class = "form-control-label"
        helper.form_tag = False  # Prevent crispy form from adding form tag. We do this manually in HTML to get more control over the submit button.
        layout = helper.layout = Layout()
        # Apply placeholders and prepended text to input
        # https://stackoverflow.com/questions/13482753/use-field-label-as-placeholder-in-django-crispy-forms/23971724#23971724

        for field_name, field in self.fields.items():
            item = PrependedText(
                field_name,
                self.prepend_text.get(field_name),
                placeholder=field.empty_value,
            )

            layout.append(item)

        return helper

    def clean_email(self):
        email = self.cleaned_data["email"]

        try:
            get_user_model().objects.get(email=email)
        except get_user_model().DoesNotExist:
            return email

        raise ValidationError(self.error_messages["duplicate_email"])


class PasswordResetForm(forms.PasswordResetForm):
    prepend_text = {
        # field_name: prepend_text
        "email": "<i class='fa fa-user'></i>",
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["email"].empty_value = "test@example.com"

    @property
    def helper(self):
        helper = FormHelper(self)
        helper.label_class = "form-control-label"
        helper.form_tag = False  # Prevent crispy form from adding form tag. We do this manually in HTML to get more control over the submit button.
        layout = helper.layout = Layout()
        # Apply placeholders and prepended text to input
        # https://stackoverflow.com/questions/13482753/use-field-label-as-placeholder-in-django-crispy-forms/23971724#23971724

        for field_name, field in self.fields.items():
            item = PrependedText(
                field_name,
                self.prepend_text.get(field_name),
                placeholder=field.empty_value,
            )

            layout.append(item)
        return helper

    def send_mail(
        self,
        subject_template_name,
        email_template_name,
        context,
        from_email,
        to_email,
        *args,
        **kwargs,
    ):

        template_id = "d-9347c41146c648d08662832c21bee312"  # Password reset template ID

        reset_url = f"""{context.get("protocol")}://{context.get("domain")}{reverse(
            "users:password_reset_confirm",
            kwargs=dict(uidb64=context.get("uid"), token=context.get("token")),
        )}"""
        send_mail(
            to_email=to_email,
            template_id=template_id,
            data={to_email: {"reset_url": reset_url}},
            from_email=from_email,
        )


class SetPasswordForm(forms.SetPasswordForm):
    prepend_text = {
        # field_name: prepend_text
        "new_password1": "<i class='fa fa-key'></i>",
        "new_password2": "<i class='fa fa-key'></i>",
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["new_password1"].empty_value = "**********"
        self.fields["new_password2"].empty_value = "**********"

    @property
    def helper(self):
        helper = FormHelper(self)
        helper.label_class = "form-control-label"
        helper.form_tag = False  # Prevent crispy form from adding form tag. We do this manually in HTML to get more control over the submit button.
        helper.form_show_errors = True
        layout = helper.layout = Layout()
        # Apply placeholders and prepended text to input
        # https://stackoverflow.com/questions/13482753/use-field-label-as-placeholder-in-django-crispy-forms/23971724#23971724

        for field_name, field in self.fields.items():
            item = PrependedText(
                field_name,
                self.prepend_text.get(field_name),
                placeholder=field.empty_value,
            )

            layout.append(item)

        return helper

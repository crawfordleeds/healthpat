from django.db import models

from crawfish.models import EmailField, BaseModel


class Feedback(BaseModel):
    class Meta:
        verbose_name = "Feedback"
        verbose_name_plural = "Feedback"

    email = EmailField(
        max_length=255,
        unique=False,  # Do not enforce unique email address so as to allow >1 feedback records per email
    )
    message = models.TextField()

    def __str__(self):
        return f"Feedback(email: '{self.email}', message: '{self.message[:10]}')"

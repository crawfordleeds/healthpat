from rest_framework import permissions
from rest_framework.generics import CreateAPIView

from .serializers import FeedbackSerializer


class FeedbackView(CreateAPIView):

    permission_classes = (permissions.AllowAny,)
    serializer_class = FeedbackSerializer

INVALID_EMAILS = (
    "test",
    "kfdjsdfds@fdsjfsd",
    "test@test",
    "723854835t",
    897235482,
    0,
    None,
    [],
    {},
)
VALID_EMAIL = "test@test.com"

INVALID_MESSAGES = (
    None,
    [],
    {},
)
VALID_MESSAGE = "Some Message 1234564327".join(["" for i in range(10000)])

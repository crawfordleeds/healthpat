from .constants import INVALID_EMAILS, INVALID_MESSAGES, VALID_EMAIL, VALID_MESSAGE


class BaseFeedbackTests:
    @classmethod
    def setUpTestData(cls):
        cls.invalid_emails = INVALID_EMAILS
        cls.valid_email = VALID_EMAIL

        cls.valid_message = VALID_MESSAGE
        cls.invalid_messages = INVALID_MESSAGES

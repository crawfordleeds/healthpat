from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from . import BaseFeedbackTests


class FeedbackViewTests(BaseFeedbackTests, APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.URL = reverse("feedback:feedback")
        super().setUpTestData()

    def test_post(self):
        """Test a valid post request"""
        data = {"email": self.valid_email, "message": self.valid_message}
        resp = self.client.post(self.URL, data=data)
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(resp.json().keys().__len__(), 2)
        self.assertTrue("email" in resp.json().keys())
        self.assertTrue("message" in resp.json().keys())
        for key in resp.json().keys():
            self.assertEqual(resp.json().get(key), data.get(key))

    def test_post_invalid_email(self):
        """Test a post request with invalid parameters"""
        for email in self.invalid_emails:
            data = {"email": email if email else "", "message": self.valid_message}
            resp = self.client.post(
                self.URL, data=data, headers={"Content-Type": "application/json"}
            )
            self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(resp.json().keys().__len__(), 1)
            self.assertEqual(resp.json().get("email").__len__(), 1)
            self.assertIsInstance(resp.json().get("email")[0], str)

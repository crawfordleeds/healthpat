from django.test import TestCase

from rest_framework.exceptions import ValidationError

from healthpat.feedback.models import Feedback
from healthpat.feedback.serializers import FeedbackSerializer

from . import BaseFeedbackTests


class FeedbackSerializerTests(BaseFeedbackTests, TestCase):
    def test_invalid_email(self):
        for email in self.invalid_emails:
            serializer = FeedbackSerializer(
                data={"email": email, "message": self.valid_message}
            )
            with self.assertRaises(ValidationError) as ve:
                serializer.is_valid(raise_exception=True)
            detail = ve.exception.detail
            self.assertEqual(detail.items().__len__(), 1)
            if (
                type(email) == str
                or type(email) == dict
                or type(email) == list
                or type(email) == int
            ):
                self.assertEqual(
                    detail.get("email")[0].__str__(), "Enter a valid email address."
                )
                self.assertEqual(detail.get("email")[0].code, "invalid")
            elif type(email) == type(None):
                self.assertEqual(
                    detail.get("email")[0].__str__(), "This field may not be null."
                )
                self.assertEqual(detail.get("email")[0].code, "null")
            else:
                raise Exception(f"Missing test for email with type f{type(email)}")

    def test_invalid_message(self):
        for message in self.invalid_messages:
            serializer = FeedbackSerializer(
                data={"email": self.valid_email, "message": message}
            )
            with self.assertRaises(ValidationError) as ve:
                serializer.is_valid(raise_exception=True)
            detail = ve.exception.detail
            self.assertEqual(detail.items().__len__(), 1)
            if type(message) == dict or type(message) == list:
                self.assertEqual(
                    detail.get("message")[0].__str__(), "Not a valid string."
                )
                self.assertEqual(detail.get("message")[0].code, "invalid")
            elif type(message) == type(None):
                self.assertEqual(
                    detail.get("message")[0].__str__(), "This field may not be null."
                )
                self.assertEqual(detail.get("message")[0].code, "null")
            else:
                raise Exception(f"Missing test for message with type f{type(message)}")

    def test_all_invalid(self):
        data = {"email": self.valid_email, "message": self.valid_message}
        serializer = FeedbackSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.assertEqual(serializer.data, data)

    def test_create(self):
        email = self.valid_email
        message = self.valid_message
        data = {"email": email, "message": message}
        serializer = FeedbackSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        feedback = serializer.save()
        self.assertEqual(feedback.__class__, Feedback)
        self.assertEqual(feedback.email, email)
        self.assertEqual(feedback.message, message)

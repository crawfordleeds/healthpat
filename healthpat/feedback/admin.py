from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered

from .models import Feedback


class FeedbackAdmin(admin.ModelAdmin):

    list_display = (
        "email",
        "message_shortened",
        "created_at",
        "updated_at",
    )

    def message_shortened(self, obj):
        """return only the first 10 chars of the message"""
        return obj.message[:10]

    fieldsets = ((None, {"fields": ("email", "message")}),)
    search_fields = ["email"]


try:
    admin.site.register(Feedback, FeedbackAdmin)
except AlreadyRegistered:
    pass

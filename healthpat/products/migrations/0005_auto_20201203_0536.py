# Generated by Django 3.1.3 on 2020-12-03 05:36

import cloudinary.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0004_auto_20201129_0316'),
    ]

    operations = [
        migrations.AddField(
            model_name='products',
            name='image',
            field=cloudinary.models.CloudinaryField(default='', max_length=255, verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='products',
            name='name',
            field=models.CharField(max_length=300, unique=True, verbose_name='Product Name'),
        ),
    ]

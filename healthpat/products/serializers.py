from rest_framework import serializers

from .models import Products, Tags


class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = "__all__"

    def to_representation(self, instance):
        return instance.name


class ProductsSerializer(serializers.ModelSerializer):
    tags = TagsSerializer(many=True, required=False)

    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        return obj.image.public_id

    class Meta:
        model = Products
        fields = (
            "objectID",
            "name",
            "tags",
            "image",
        )

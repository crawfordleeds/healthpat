import uuid
import logging
import json
from django.db import models
from django.utils.translation import ugettext_lazy as _

import cloudinary
from cloudinary.models import CloudinaryField
from healthpat.algolia.tasks import delete_object, save_object
from crawfish.models import BaseModel

from .apps import ProductsConfig as Config

logger = logging.getLogger(__name__)


class Tags(BaseModel):
    class Meta:
        verbose_name = "Tags"
        verbose_name_plural = "Tags"

    name = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.name}"


class Products(BaseModel):
    class Meta:
        verbose_name = "Products"
        verbose_name_plural = "Products"

    uuid = models.UUIDField(_("uuid"), default=uuid.uuid4, editable=False, unique=True)
    objectID = models.CharField(_("Algolia Object ID"), max_length=300, blank=True)
    name = models.CharField(_("Product Name"), max_length=300, unique=True)
    tags = models.ManyToManyField(Tags, blank=True)
    image = CloudinaryField("image", folder="healthpat/products/", default="")

    def save(self, save_to_algolia=True, *args, **kwargs):
        super().save(*args, **kwargs)

        if save_to_algolia:
            algolia_kwargs = {
                "app_name": Config.name,
                "model_name": self.__class__.__name__,
                "record_id": self.id,
            }
            logger.info(
                f"Sending save request to save record to algolia. kwargs: {json.dumps(algolia_kwargs)}"
            )
            save_object.delay(**algolia_kwargs)

    def delete(self, *args, **kwargs):
        if self.objectID:
            """Delete the record from algolia only if an Algolia object ID exists"""
            delete_object(self.objectID)

        # If this is the last product to use a specific Cloudinary image, delete the image in cloudinary
        if Products.objects.filter(image=self.image).count() < 2:
            cloudinary.uploader.destroy(
                self.image.public_id
            ) if self.image.public_id else None

        return super().delete(*args, **kwargs)

    def to_json(self):
        from .serializers import ProductsSerializer

        serializer = ProductsSerializer(self)

        # serializer.is_valid()
        return serializer.data

    def __str__(self):
        return f"Product(name: '{self.name}')"

from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered

from .models import Products, Tags


class ProductsAdmin(admin.ModelAdmin):

    list_display = (
        "id",
        "uuid",
        "objectID",
        "name",
        "all_tags",
        "created_at",
        "updated_at",
    )

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "tags",
                    "objectID",
                    "image",
                )
            },
        ),
    )
    search_fields = (
        "name",
        "uuid",
        "objectID",
    )
    filter_horizontal = ("tags",)
    readonly_fields = ("objectID",)

    def all_tags(self, obj):
        """Return a comma delimited string of tags"""
        return ", ".join([t.name for t in obj.tags.all()])


class TagsAdmin(admin.ModelAdmin):

    list_display = ("name", "created_at", "updated_at")
    fieldset = (None, {"fields": ("name",)})
    search_fields = ("name",)


try:
    admin.site.register(Products, ProductsAdmin)
    admin.site.register(Tags, TagsAdmin)
except AlreadyRegistered:
    pass

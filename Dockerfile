FROM python:3.8-alpine as base

WORKDIR /app

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1  # Prevents Python from writing pyc files to disc
ENV PYTHONUNBUFFERED 1  # Prevents Python from buffering stdout and stderr
ENV DJANGO_SETTINGS_MODULE config.settings.remote
ENV DJANGO_READ_DOT_ENV_FILE 0
ENV DATABASE_URL postgres://postgres:postgres@db:5432/healthpat
ENV SENDGRID_API_KEY ""
ENV ALGOLIA_API_KEY "randomkey"
ENV ALGOLIA_APPLICATION_ID "someid"
ENV COINMARKETCAP_API_KEY ""

# Vue App env vars
ENV VUE_APP_ALGOLIA_APPLICATION_ID PT8V4E0O9D
ENV VUE_APP_ALGOLIA_API_KEY 178bf7cb4d70bca387af7ca0ac7aa56d
ENV VUE_APP_INDEX_NAME PRODUCTS_DEV

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev build-base curl \
    && apk add --update nodejs nodejs-npm yarn \
    && apk add postgresql-dev postgresql-client bash # build-base required for libsass. alpine-sdk also works.

# Copy project
COPY . .
RUN pip install -r requirements.txt
#RUN pip install django

RUN apk del build-deps

RUN yarn install
WORKDIR /app/vue_frontend
RUN yarn install && yarn build --mode development
WORKDIR /app

# collect static files
RUN python manage.py collectstatic --noinput --ignore libs/imagesloaded/sandbox/background/*
RUN python manage.py compress --force

# Run gunicorn
FROM base as web_server
# Add and run as non-root user
RUN adduser -D myuser
USER myuser
CMD gunicorn config.wsgi:application --bind 0.0.0.0:$PORT

FROM base as release
CMD python manage.py migrate

FROM base as worker
CMD celery -A config.celery_app.app worker --beat --loglevel=info --concurrency=4

FROM base as test
CMD python manage.py test --settings=config.settings.remote

FROM base as web_server_staging
RUN yarn install
WORKDIR /app/vue_frontend
RUN yarn install && yarn build
WORKDIR /app

RUN python manage.py collectstatic --noinput --ignore libs/imagesloaded/sandbox/background/*
RUN python manage.py compress --force

# Add and run as non-root user
RUN adduser -D myuser
USER myuser

CMD gunicorn config.wsgi:application --bind 0.0.0.0:$PORT

FROM base as web_server_prod
ENV VUE_APP_INDEX_NAME PRODUCTS_PROD
RUN yarn install
WORKDIR /app/vue_frontend
RUN yarn install && yarn build
WORKDIR /app

RUN python manage.py collectstatic --noinput --ignore libs/imagesloaded/sandbox/background/*
RUN python manage.py compress --force

# Add and run as non-root user
RUN adduser -D myuser
USER myuser

CMD gunicorn config.wsgi:application --bind 0.0.0.0:$PORT


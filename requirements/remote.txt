# Install the base dependencies first
-r ./base.txt

brotli==1.0.7  # Brotli compression for whitenoise
gunicorn==20.0.4
whitenoise==5.1.0

#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name="healthpat",
    packages=find_packages(exclude=["tests", "tests.*"]),
    version="0.0.30",  # don't change this manually, use bumpversion instead
    description="HealthPat",
    author="Crawford Leeds",
    author_email="crawford@crawfordleeds.com",
    url="healthpat.co",
    scripts=["manage.py"],
    classifiers=[
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
    ],
)

[![](https://res.cloudinary.com/dmuligbfy/image/upload/c_scale,w_150,r_20/v1607112196/healthpat/healthpat_blue.png)](https://healthpat.co)
# Healthpat

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/crawfordleeds/healthpat)
![pipeline status](https://gitlab.com/crawfordleeds/healthpat/badges/master/pipeline.svg)

[![coverage report](https://gitlab.com/crawfordleeds/healthpat/badges/master/coverage.svg)](https://gitlab.com/crawfordleeds/healthpat/-/commits/master)
[![Codecov](https://img.shields.io/codecov/c/gl/crawfordleeds/healthpat?label=Codecov%20Coverage)](https://codecov.io/gl/crawfordleeds/healthpat/)

Health Pat helps Americans find big savings on their prescription medication in Mexico.

[Try it today](https://www.healthpat.co)

Are we missing your medication? Request it confidentially [here](https://www.healthpat.co/request)

## Running with Docker

### Build

```bash
docker build -t web:latest .
```

### Run with env Configured

```bash
docker run -d --name healthpat -e "PORT=8765" -p 8007:8765 web:latest
```

### Remove All Docker Containers

```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```


## Setting up New app

1. Create App in Heroku
2. Set stack to container: `heroku stack:set container -a {{ app_name }}`
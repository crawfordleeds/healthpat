import HealthPatAPI from "@/lib/core/client";
import BaseForm from "@/lib/forms";

export default function FeedbackForm(data) {
  BaseForm.call(this, data);
  this.api = HealthPatAPI;
}

FeedbackForm.prototype = Object.create(BaseForm.prototype);
FeedbackForm.prototype.constructor = FeedbackForm;

FeedbackForm.prototype.submit = async function submit() {
  return new Promise((resolve, reject) => {
    this.api.Feedback.create(this.formData())
      .then((res) => {
        this.onSuccess(res.data);
        resolve(res);
      })
      .catch((err) => {
        this.onFail(err.response.data);
        reject(err);
      });
  });
};

import Vue from "vue";
import FeedbackButton from "./FeedbackButton.vue";
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  Vue,
  dsn:
    "https://76884a7b562e4169a9acf95401ef5eda@o304185.ingest.sentry.io/5545438",
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
});

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(FeedbackButton),
}).$mount("#feedback-button");

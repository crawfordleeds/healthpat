import { HealthPatAPI } from "@/api-client";
import axios from "./axios";

export default Object.freeze(
  new HealthPatAPI({ environment: process.env.NODE_ENV, fetcher: axios })
);

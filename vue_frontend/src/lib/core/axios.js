import getBaseUrl from "./utils";
import axios from "axios";
import Cookies from "js-cookie";

const csrfToken = Cookies.get("csrftoken");
axios.defaults.headers.common["X-CSRFToken"] = csrfToken;
axios.defaults.baseURL = getBaseUrl();
export default axios;

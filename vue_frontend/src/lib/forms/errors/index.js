function BaseErrors() {
  this.errors = {};
}

BaseErrors.prototype.has = function has(field) {
  // Check if errors exist in a given field
  let hasError = this.errors.hasOwnProperty(field);
  return hasError;
};

BaseErrors.prototype.any = function any() {
  // Check if any errors
  return Object.keys(this.errors).length > 0;
};

BaseErrors.prototype.get = function get(field) {
  // Retrieve error message on a field
  if (this.errors[field]) {
    return this.errors[field][0];
  }
};

BaseErrors.prototype.record = function record(errors) {
  // Record errors
  this.errors = errors;
  console.log("errors recorded:");
  console.log(this.errors);
};

BaseErrors.prototype.clear = function clear(field) {
  // Clear errors on one field or all fields
  if (field) {
    console.log(`deleting field ${field}`);
    delete this.errors[field];
  } else {
    console.log("clear all field errors");
    this.errors = {};
  }
};

export default BaseErrors;

import BaseErrors from "./errors";

function BaseForm(data) {
  this.originalData = data;
  for (let field in data) {
    this[field] = data[field];
  }
  this.errors = new BaseErrors();
}

BaseForm.prototype.formData = function formData() {
  let data = {};
  for (let property in this.originalData) {
    data[property] = this[property];
  }
  return data;
};

BaseForm.prototype.onSuccess = function onSuccess(data) {
  console.log("in onSuccess BaseForm method");
  console.log(data);
  this.reset();
};

BaseForm.prototype.onFail = function onFail(errors) {
  console.log("on fail");
  console.log(errors);
  this.errors.record(errors);
};

BaseForm.prototype.reset = function reset() {
  console.log("reset form");
  for (let field in this.originalData) {
    this[field] = this.originalData[field];
  }
  this.errors.clear();
};

export default BaseForm;

import Feedback from "./feedback";
/**
 * Base class for API requests. Abstract class, do not instantiate
 */

class HealthPatAPI {
  constructor(config) {
    this.environment = config.environment;
    this.Feedback = new Feedback(config.fetcher);
  }
}

export { HealthPatAPI };

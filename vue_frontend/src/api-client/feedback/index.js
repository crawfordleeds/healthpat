class Feedback {
  constructor(fetcher) {
    this.request = fetcher;
    this.endpoint = "/v1/feedback/";
  }

  async create(data) {
    // Send feedback to admin
    return this.request.post(this.endpoint, data);
  }
}

export default Feedback;

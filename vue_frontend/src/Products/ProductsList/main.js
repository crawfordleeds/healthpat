import Vue from "vue";
import Cloudinary from "cloudinary-vue";
import ProductsList from "./ProductsList.vue";
import InstantSearch from "vue-instantsearch";
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";

Vue.use(InstantSearch);

Vue.use(Cloudinary, {
  configuration: { cloudName: "dmuligbfy" },
});

Sentry.init({
  Vue,
  dsn:
    "https://76884a7b562e4169a9acf95401ef5eda@o304185.ingest.sentry.io/5545438",
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
});

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(ProductsList),
}).$mount("#products-list");

const BundleTracker = require("webpack-bundle-tracker");

const pages = {
  products_list: {
    entry: "./src/Products/ProductsList/main.js",
    chunks: ["chunk-vendors"],
  },
  feedback_button: {
    entry: "./src/FeedbackButton/main.js",
    chunks: ["chunks-vendors"],
  },
};

module.exports = {
  lintOnSave: false, // eslint sucks
  pages: pages,
  filenameHashing: true,
  productionSourceMap: false,
  publicPath: process.env.NODE_ENV === "local" ? "http://localhost:8080" : "",
  outputDir: "../healthpat/static/vue/",

  chainWebpack: (config) => {
    config.optimization.splitChunks({
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "chunk-vendors",
          chunks: "all",
          priority: 1,
        },
      },
    });

    Object.keys(pages).forEach((page) => {
      config.plugins.delete(`html-${page}`);
      config.plugins.delete(`preload-${page}`);
      config.plugins.delete(`prefetch-${page}`);
    });

    config.plugin("BundleTracker").use(BundleTracker, [
      {
        // WHEN WE GET TO >v1 of webpack-bundle-tracker
        // relativePath: true, // only applicable with >v1 of webpack-bundle-tracker
        // path: ".",
        // filename: "webpack-stats.json",

        filename: "webpack-stats.json",
      },
    ]);

    // Uncomment below to analyze bundle sizes
    // config.plugin("BundleAnalyzerPlugin").use(BundleAnalyzerPlugin);

    // config.resolve.alias
    // .set('__STATIC__', 'static')

    config.devServer
      .public("http://localhost:8080")
      .host("localhost")
      .port(8080)
      .hotOnly(true)
      .watchOptions({ poll: 1000 })
      .https(false)
      .headers({ "Access-Control-Allow-Origin": ["*"] });
  },
};
